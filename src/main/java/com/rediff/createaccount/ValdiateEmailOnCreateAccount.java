package com.rediff.createaccount;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.rediff.util.Common;

public class ValdiateEmailOnCreateAccount extends Common {
	@BeforeSuite
	public void loadFiles() throws IOException {
		init();
	}

	@BeforeMethod
	public void openBrowsers() {
		openBrowser();
	}

	@AfterMethod
	public void closeBrowsers() {
		closeBrowser();
	}

	@Test
	public void valdiateEmailAvailability() throws InterruptedException, IOException {
		try {
			driver.findElement(By.linkText(obj.getProperty("createAccountLinkText"))).click();
			driver.findElement(By.xpath(obj.getProperty("firstNameInputXpath"))).sendKeys("ashok");
			driver.findElement(By.xpath(obj.getProperty("emailInputXpath"))).sendKeys("ashok");
			driver.findElement(By.xpath(obj.getProperty("checkAvailabulityButtonXpath"))).click();

			Thread.sleep(500);
			WebDriverWait wait = new WebDriverWait(driver, 50);
			String actualEmailAvailableErrorMsg = wait
					.until(ExpectedConditions.visibilityOf(
							driver.findElement(By.xpath(obj.getProperty("emailAvailableErrorMessageXpath")))))
					.getText();
			String expectedEmailAvailableErrorMsg = "Sorry, the ID that you are looking for is taken.";

			Assert.assertEquals(actualEmailAvailableErrorMsg, expectedEmailAvailableErrorMsg);
			log.debug("Valiate Emai id available message");
		} catch (Exception e) {
			captureScreenshot();
			log.debug(e.toString());
		}
	}
}
