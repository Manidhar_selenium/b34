package com.rediff.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeSuite;

public class Common {
	public WebDriver driver = null;
	public Properties env = null;
	public Properties obj = null;
	public InputStream file1 = null;
	public InputStream file2 = null;
	public Logger log = null;
	String path = System.getProperty("user.dir");

	public void init() throws IOException {

		log = Logger.getLogger("rootLogger");
		log.debug("Logger is configured");
		file1 = new FileInputStream(path + "/resources/Environment.properties");
		env = new Properties();
		env.load(file1);
		log.debug("Environment properties file loaded");

		file2 = new FileInputStream(path + "/resources/Objects.properties");
		obj = new Properties();
		obj.load(file2);
		log.debug("Objects properties file loaded");

	}

	public void openBrowser() {
		String osName = System.getProperty("os.name").toLowerCase();
		System.out.println("osName:"+osName);
		if (osName.contains("mac")) {
			System.setProperty("webdriver.chrome.driver", path + "/resources/drivers/chrome/mac/chromedriver");
			driver = new ChromeDriver();
			driver.get(env.getProperty("url"));
			log.debug("Opened Chrome browser");
		} else if (osName.contains("windows")) {
			System.setProperty("webdriver.chrome.driver", path + "/resources/drivers/chrome/windows/chromedriver.exe");
			driver = new ChromeDriver();
			driver.get(env.getProperty("url"));
			log.debug("Opened Chrome browser");
		}
	}

	public void closeBrowser() {
		driver.close();
		log.debug("Closing the Browser");
	}

	public void captureScreenshot() throws IOException {
		TakesScreenshot screen = (TakesScreenshot) driver;
		File capturedWebContent = screen.getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(capturedWebContent, new File(path + "/resources/screenshots/Screen1.png"));

	}
	/*
	 * @BeforeSuite public void closeFileConnections() throws IOException { env =
	 * null; obj = null; file1 = null; file2 = null; }
	 */
}
