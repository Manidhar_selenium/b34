package com.rediff.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ReadExcel extends Common{
	public Object[][] readData() throws EncryptedDocumentException, IOException {
		InputStream excelFile = new FileInputStream( path+"/resources/LoginTestData.xlsx");

		Workbook workbook = WorkbookFactory.create(excelFile);
		Sheet sheet = workbook.getSheetAt(0);

		int lastRowno = sheet.getLastRowNum();
		//log.debug("lastRowno: " + lastRowno);
		int toatlRowCount = lastRowno + 1;

		Row row = sheet.getRow(0);
		int totalColumnCount = row.getLastCellNum();

		Object o[][] = new Object[toatlRowCount][totalColumnCount];
		// Rows
		for (int i = 0; i < toatlRowCount; i++) {
			row = sheet.getRow(i);
			totalColumnCount = row.getLastCellNum();
			for (int j = 0; j < totalColumnCount; j++) {
				Cell cell=row.getCell(j);
				o[i][j]=cell.getStringCellValue();
			}
		}
		return o;
	}

	public static void main(String[] args) throws EncryptedDocumentException, IOException {
		ReadExcel readExcel = new ReadExcel();
		readExcel.readData();
	}

}
