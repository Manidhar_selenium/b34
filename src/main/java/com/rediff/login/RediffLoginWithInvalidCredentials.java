package com.rediff.login;

import java.io.IOException;
import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.rediff.util.Common;
import com.rediff.util.ReadExcel;

public class RediffLoginWithInvalidCredentials extends Common {
	@BeforeSuite
	public void loadFiles() throws IOException {
		init();
	}

	@BeforeMethod
	public void openBrowsers() {
		openBrowser();
	}

	@Test(priority = 1)
	public void validatePageTitle() throws IOException {
		try {
		String actualPageTitle = driver.getTitle();
		String expectedPageTitle = "Rediff.com: News | Rediffmail | Stock Quotes | Shopping";
		System.out.println(actualPageTitle.length());
		Assert.assertEquals(actualPageTitle, expectedPageTitle);
		log.debug("Verified Page title");
		// Assert.assertTrue(actualPageTitle.length() > 100,"Count of Title is not
		// >100");

		// Assert.assertFalse(actualPageTitle.length() < 100);
		}catch(Exception e) {
			captureScreenshot();
			log.debug(e.toString());
		}
	}

	@Test(priority = 2, dependsOnMethods = "validatePageTitle", enabled = true, dataProvider = "data")
	public void loginWithInvalidCredentials(String username, String password) throws IOException {
		try {
			driver.findElement(By.linkText(obj.getProperty("signInLinkText"))).click();
			driver.findElement(By.cssSelector(obj.getProperty("emailInputCSS"))).sendKeys(username);
			driver.findElement(By.cssSelector(obj.getProperty("passwordInputCSS"))).sendKeys(password);
			driver.findElement(By.name(obj.getProperty("goButtonName"))).click();

			WebDriverWait wait = new WebDriverWait(driver, 30);
			String actualErrorMessage = wait
					.until(ExpectedConditions
							.visibilityOf(driver.findElement(By.xpath(obj.getProperty("errorMessageXpath")))))
					.getText();

			String expectedErrorMessage = "Wrong username and password combination.";
			Assert.assertEquals(actualErrorMessage, expectedErrorMessage);
			log.debug("Verified Login with invalid credentials : un: " + username + "--- pass: " + password);
		} catch (Exception e) {
            //1 capture screenshot
			captureScreenshot();
			// 2 print the exception in Log file
			log.debug(e.toString());
		}
	}

	@AfterMethod
	public void closeBrowsers() {
		closeBrowser();
	}

	@DataProvider
	public Object[][] data() throws EncryptedDocumentException, IOException {

		ReadExcel excelData = new ReadExcel();
		Object o[][] = excelData.readData();
		log.debug("In DataProvider method");
		return o;
	}

}
